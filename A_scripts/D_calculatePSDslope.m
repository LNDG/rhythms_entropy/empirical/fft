%% Calculate PSD slope, perform PSD CBPA

    IDs_YA = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    IDs_OA = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

%% load PSD

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

    figure; 
    subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
    subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

    commonYA = info.IDs_all(ismember(info.IDs_all, IDs_YA));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs_OA));

    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs_YA, commonYA);
    idx_OA_mse = ismember(IDs_OA, commonOA);

%% linear fit between 1-30 Hz (excluding 7-13 Hz range): log-log space
% shallower slope --> increasing neural irregularity

    nonAlphaSub30 = find(grandavg_EC.freq <7 | grandavg_EC.freq >13 & grandavg_EC.freq <=64);

    figure; 
    subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm(:,:,nonAlphaSub30),2))); title('Eyes Closed');
    subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(:,:,nonAlphaSub30),2))); title('Eyes Open');

    figure; 
    hold on; plot(log10(grandavg_EC.freq(nonAlphaSub30)),squeeze(nanmean(nanmean(log10(grandavg_EC.powspctrm(:,:,nonAlphaSub30)),2),1))); title('Eyes Closed');
    plot(log10(grandavg_EC.freq(nonAlphaSub30)),squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(:,:,nonAlphaSub30)),2),1))); title('Eyes Open');
    
    linFit_2_30_EC = []; linFit_2_30_EC_int = [];
    for indID = 1:size(grandavg_EC.powspctrm,1)
        for indChan = 1:60
            x = log10(grandavg_EC.freq(nonAlphaSub30))';
            y = log10(squeeze(grandavg_EC.powspctrm(indID,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30_EC(indID, indChan) = pv(1);
            linFit_2_30_EC_int(indID, indChan) = pv(2);
        end
    end

    linFit_2_30_EO = []; linFit_2_30_EO_int = [];
    for indID = 1:size(grandavg_EO.powspctrm,1)
        for indChan = 1:60
            x = log10(grandavg_EO.freq(nonAlphaSub30))';
            %x = grandavg_EO.freq(nonAlphaSub30)';
            y = log10(squeeze(grandavg_EO.powspctrm(indID,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30_EO(indID, indChan) = pv(1);
            linFit_2_30_EO_int(indID, indChan) = pv(2);
        end
    end

    save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes_loglog.mat', ...
        'linFit_2_30_EC', 'linFit_2_30_EC_int', 'linFit_2_30_EO','linFit_2_30_EO_int', 'grandavg_EC', 'grandavg_EO')

%% linear fit between 1-30 Hz (excluding 7-13 Hz range): log-normal space (used for analyses)
% shallower slope --> increasing neural irregularity

    nonAlphaSub30 = find(grandavg_EC.freq <7 | grandavg_EC.freq >13 & grandavg_EC.freq <=64);

    figure; 
    subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm(:,:,nonAlphaSub30),2))); title('Eyes Closed');
    subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(:,:,nonAlphaSub30),2))); title('Eyes Open');

    figure; 
    hold on; plot(grandavg_EC.freq(nonAlphaSub30),squeeze(nanmean(nanmean(log10(grandavg_EC.powspctrm(:,:,nonAlphaSub30)),2),1))); title('Eyes Closed');
    plot(grandavg_EC.freq(nonAlphaSub30),squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(:,:,nonAlphaSub30)),2),1))); title('Eyes Open');
    
    linFit_2_30_EC = []; linFit_2_30_EC_int = [];
    for indID = 1:size(grandavg_EC.powspctrm,1)
        for indChan = 1:60
            %x = log10(grandavg_EC.freq(nonAlphaSub30))';
            x = grandavg_EC.freq(nonAlphaSub30)';
            y = log10(squeeze(grandavg_EC.powspctrm(indID,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30_EC(indID, indChan) = pv(1);
            linFit_2_30_EC_int(indID, indChan) = pv(2);
        end
    end

    linFit_2_30_EO = []; linFit_2_30_EO_int = [];
    for indID = 1:size(grandavg_EO.powspctrm,1)
        for indChan = 1:60
            %x = log10(grandavg_EO.freq(nonAlphaSub30))';
            x = grandavg_EO.freq(nonAlphaSub30)';
            y = log10(squeeze(grandavg_EO.powspctrm(indID,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30_EO(indID, indChan) = pv(1);
            linFit_2_30_EO_int(indID, indChan) = pv(2);
        end
    end

    save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat', ...
        'linFit_2_30_EC', 'linFit_2_30_EC_int', 'linFit_2_30_EO','linFit_2_30_EO_int', 'grandavg_EC', 'grandavg_EO')

    % figure; 
    % subplot(1,2,1); imagesc(linFit_2_30_EC, [0 -.5]) % some fits across the 1-30 Hz range are positive due to the presence of high freq power
    % subplot(1,2,2); imagesc(linFit_2_30_EO, [0 -.5]); colorbar

    figure; hold on;
    plot(squeeze(nanmean(linFit_2_30_EC,2)))
    plot(squeeze(nanmean(linFit_2_30_EO,2)))

%% topography of slope fits

    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [-.03 .03];

    figure;
    subplot(2,2,1)
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EC(idx_YA_fft,:),1))';
        ft_topoplotER(cfg,plotData);
        title('Slopes EC, YA')
    subplot(2,2,2)
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft,:),1))';
        ft_topoplotER(cfg,plotData);
        title('Slopes EO, YA')
    subplot(2,2,3)
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EC(idx_OA_fft,:),1))'-squeeze(nanmean(linFit_2_30_EC(idx_YA_fft,:),1))';
        ft_topoplotER(cfg,plotData);
        title('Slopes EC, OA')
    subplot(2,2,4)
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft,:),1))'-squeeze(nanmean(linFit_2_30_EO(idx_YA_fft,:),1))';
        ft_topoplotER(cfg,plotData);
        title('Slopes EO, OA-YA')

%% CBPA of slope differences: OA vs. YA

    CBPAstruct{1} = grandavg_EO;
    %CBPAstruct{1}.dimord = 'subj_chan';
    CBPAstruct{1}.freq = 1;
    CBPAstruct{1}.powspctrm = linFit_2_30_EO(idx_YA_fft,:);

    CBPAstruct{2} = grandavg_EO;
    %CBPAstruct{2}.dimord = 'subj_chan';
    CBPAstruct{2}.freq = 1;
    CBPAstruct{2}.powspctrm = linFit_2_30_EO(idx_OA_fft,:);

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct{1}.label;

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_indepsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1});

    N_1 = size(CBPAstruct{2}.powspctrm,1);
    N_2 = size(CBPAstruct{1}.powspctrm,1);
    cfgStat.design = zeros(1,N_1+N_2);
    cfgStat.design(1,1:N_1) = 1;
    cfgStat.design(1,N_1+1:end) = 2;

    cfgStat.parameter = 'powspctrm';
    [stat] = ft_freqstatistics(cfgStat, CBPAstruct{2}, CBPAstruct{1});

    % calculate contrast to zero

    CBPAstruct_all = grandavg_EO;
    CBPAstruct_all.freq = 1;
    CBPAstruct_all.powspctrm = linFit_2_30_EO(:,:);

    CBPAstruct_zero = grandavg_EO;
    CBPAstruct_zero.freq = 1;
    CBPAstruct_zero.powspctrm = zeros(size(linFit_2_30_EO));

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_depsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct_all);

    subj = size(CBPAstruct_all.powspctrm,1);
    conds = 2;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    cfgStat.parameter = 'powspctrm';
    [stat_zero] = ft_freqstatistics(cfgStat, CBPAstruct_all,CBPAstruct_zero);

    % save stat output
    pn.outFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/';
    save([pn.outFolder, 'D2_CBPASlopeAge.mat'], 'stat', 'stat_zero')

%% plot PSD slope topographies alongside age difference

    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    subplot(1,2,1);
        cfg = [];
        cfg.layout = 'acticap-64ch-standard2.mat';
        cfg.parameter = 'powspctrm';
        cfg.comment = 'no';
        cfg.colorbar = 'SouthOutside';
    %     cfg.highlight = 'yes';
    %     cfg.highlightchannels = CBPAstruct{1}.label(stat.mask);
    %     cfg.highlightcolor = [1 0 0];
        cfg.zlim = [-.03 .03];
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EO,1))';
        ft_topoplotER(cfg,plotData);
        title('Spectral slopes across age')
    subplot(1,2,2);
        cfg = [];
        cfg.layout = 'acticap-64ch-standard2.mat';
        cfg.parameter = 'powspctrm';
        cfg.comment = 'no';
        cfg.colorbar = 'SouthOutside';
        cfg.zlim = [-7 7];
        cfg.highlight = 'yes';
        cfg.highlightchannels = CBPAstruct{1}.label(stat.mask);
        cfg.highlightcolor = [1 0 0];
        plotData.powspctrm = stat.stat;
        ft_topoplotER(cfg,plotData);
        title('Spectral slopes: T values OA > YA');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
        figureName = 'F_topographyCBPASlopeAge';
        saveas(h, [pn.plotFolder, figureName], 'epsc');
        saveas(h, [pn.plotFolder, figureName], 'png');


        %% overlap between log-log and log-normal
        
        loglogData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes_loglog.mat', ...
        'linFit_2_30_EC', 'linFit_2_30_EC_int', 'linFit_2_30_EO','linFit_2_30_EO_int', 'grandavg_EC', 'grandavg_EO');
    
        lognormalData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat', ...
            'linFit_2_30_EC', 'linFit_2_30_EC_int', 'linFit_2_30_EO','linFit_2_30_EO_int', 'grandavg_EC', 'grandavg_EO');
        
        figure; scatter(squeeze(nanmean(loglogData.linFit_2_30_EO(idx_YA_fft,1:60),2)), ...
            squeeze(nanmean(lognormalData.linFit_2_30_EO(idx_YA_fft,1:60),2)));
        xlabel('Individual log-log slope fit coefficients'); ylabel('Individual log-normal coefficients')
