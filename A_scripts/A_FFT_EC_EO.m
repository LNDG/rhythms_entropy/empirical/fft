function A_FFT_EC_EO()

% Function calculates a fast-fourier transform for Resting State data:
% multitaper, with Hanning-tapering, 41 log-spaced center frequencies [2-64 Hz]
% Input data are CSD-transformed
% Input data are pseudo-segmented to 3s segments (not necessarily continuous)

%% initialize

    restoredefaultpath;
    clear all; close all; pack; clc;

%% set up paths

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
    pn.dataOut  = [pn.root, 'B_analyses/A_MSE_CSD/B_data/B_MSE_Segmented_Dim_Input/']; mkdir(pn.dataOut);
    pn.tools    = [pn.root, 'B_analyses/A_MSE_CSD/T_tools/'];
    addpath([pn.tools, 'qsub_tardis']);
    addpath([pn.tools, 'functions_mse_time/']);
    addpath([pn.tools, 'fieldtrip-20170904/']);
    ft_defaults

%% filenames

    % N = 47 YA, 52 OAs
    % 2201 - no rest available; 1213 dropped (weird channel arrangement)

    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

%% preproc the data

    fprintf('\n\nSubject directory: %s  . . .\n', pn.dataIn)
    cd(pn.dataIn)

    cond_names = {'EC'; 'EO'};

    for indID = 1:numel(IDs)
        for indCond = 1:2

        %% load dataset
            fprintf('Loading Subject %s %s: . . .\n', IDs{indID}(1:4), cond_names{indCond})
            datfile = dir(sprintf('%s_rest_EEG_Rlm_Fhl_rdSeg_Art_%s.mat', IDs{indID}(1:4), cond_names{indCond}));
            if isempty(datfile);     break;            end
            cfg.inputfile = fullfile(pn.dataIn, datfile.name);
            dataCond = load(cfg.inputfile);
            data = dataCond.data;clear dataCond;
            cfg_old = data.cfg;

        %% CSD transform
            csd_cfg = [];
            csd_cfg.elecfile = 'standard_1005.elc';
            csd_cfg.method = 'spline';
            data = ft_scalpcurrentdensity(csd_cfg, data);
            data.cfg = cfg_old; clear cfg_old;
            data.cfg.csd = csd_cfg;

        %% change the temporal trial definition (if necessary)

            for indTrial = 1:size(data.trial,2)
                sampleAmount(indTrial) = numel(data.time{indTrial});
                data.time{indTrial} = data.time{1};
            end
            deletionTrials = find(sampleAmount < 1500);
            if ~isempty(deletionTrials)
                disp(['Deleting trials ', num2str(deletionTrials)]);
                data.time(deletionTrials) = [];
                data.trial(deletionTrials) = [];
            end
            if numel(data.trial)<10
                error('Too few trials. Check accuracy of trial definition.')
            end

        %% remove A1 channel

            cfg = [];
            cfg.channel = {'all', '-A1'};
            data = ft_preprocessing(cfg, data);

        %% calculate FFT

            param.FFT.trial       = 'all';
            param.FFT.method      = 'mtmfft';       % analyze entire spectrum; multitaper
            param.FFT.output      = 'pow';          % only get power values
            param.FFT.keeptrials  = 'no';           % return individual trials
            param.FFT.taper       = 'hanning';      % use hanning windows
            param.FFT.pad         = 10;             % add padding
            param.FFT.foi         = 2.^[1:.125:6];  % frequencies of interest
            freq = ft_freqanalysis(param.FFT, data);

            %figure; imagesc(freq.powspctrm);

            pn.dataOut = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/A_spectra/'];
            if ~exist(pn.dataOut); mkdir(pn.dataOut); end

            save([pn.dataOut, sprintf('%s_%s_FFT.mat', IDs{indID}(1:4), cond_names{indCond})],'freq');
            clear data;
        end
    end


end