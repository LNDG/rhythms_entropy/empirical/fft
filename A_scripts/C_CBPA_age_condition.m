function C_CBPA_age_condition()

% Perform cluster-based permutation analysis (CBPA) on resting state PSD data

%% initialize

    restoredefaultpath;
    clear all; close all; pack; clc;

%% set up paths

    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'B_analyses/B_FFT/B_data/A_spectra/'];
    pn.dataOut  = [pn.root, 'B_analyses/B_FFT/B_data/'];
    pn.tools    = [pn.root, 'B_analyses/A_MSE_CSD_multiVariant/T_tools/'];
    addpath([pn.tools, 'fieldtrip-20170904/']);
    ft_defaults

%% load resting spectra

    load([pn.dataOut, 'B_FFT_grandavg_individual.mat'], 'grandavg_EC', 'grandavg_EO', 'info')

    ageIdx{1} = find(cellfun(@str2num, info.IDs_all, 'un', 1)<2000);
    ageIdx{2} = find(cellfun(@str2num, info.IDs_all, 'un', 1)>=2000);

    conds = {'EC'; 'EO'};

    for indAge = 1:2
        for indCond = 1:2
            CBPAstruct{indAge,indCond} = eval(['grandavg_', conds{indCond}]);
            CBPAstruct{indAge,indCond}.powspctrm = CBPAstruct{indAge,indCond}.powspctrm(ageIdx{indAge},:,:);
        end
    end

%% CBPA: within-group: EC vs. EO

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct{1,1}.label;

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_depsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1,1});

    subj = size(CBPAstruct{1,1}.powspctrm,1);
    conds = 2;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    cfgStat.parameter = 'powspctrm';
    [stat{1,1}] = ft_freqstatistics(cfgStat, CBPAstruct{1,1}, CBPAstruct{1,2});

    subj = size(CBPAstruct{2,1}.powspctrm,1);
    conds = 2;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    cfgStat.parameter = 'powspctrm';
    [stat{2,1}] = ft_freqstatistics(cfgStat, CBPAstruct{2,1}, CBPAstruct{2,2});

%% CBPA: between-group: EC: OA vs. YA; EO: OA vs. YA

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_indepsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1,1});

    N_1 = size(CBPAstruct{1,1}.powspctrm,1);
    N_2 = size(CBPAstruct{2,1}.powspctrm,1);
    cfgStat.design = zeros(1,N_1+N_2);
    cfgStat.design(1,1:N_1) = 1;
    cfgStat.design(1,N_1+1:end) = 2;

    cfgStat.parameter = 'powspctrm';
    [stat{3,1}] = ft_freqstatistics(cfgStat, CBPAstruct{2,1}, CBPAstruct{1,1});
    [stat{4,1}] = ft_freqstatistics(cfgStat, CBPAstruct{2,2}, CBPAstruct{1,2});

 %% save statistics results
    
    save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat', 'stat')