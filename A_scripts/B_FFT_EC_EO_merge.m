function B_FFT_EC_EO_merge()

% collect PSD data from all subjects and combine them into single structure

%% initialize

    restoredefaultpath;
    clear all; close all; pack; clc;

%% set up paths

    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'B_analyses/B_FFT/B_data/A_spectra/'];
    pn.dataOut  = [pn.root, 'B_analyses/B_FFT/B_data/'];
    pn.tools    = [pn.root, 'B_analyses/A_MSE_CSD_multiVariant/T_tools/'];
    addpath([pn.tools, 'fieldtrip-20170904/']);
    ft_defaults

%% filenames

    % N = 47 YA, 52 OAs
    % 2201 - no rest available; 1213 dropped (weird channel arrangement)

    info.IDs_all = {'1117';'1118'; '1120'; '1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
        '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
        '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120'; '2121'; '2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

%% collect data and merge across subjects

    cond_names = {'EC'; 'EO'};

    for indID = 1:numel(info.IDs_all)
        for indCond = 1:2
            % load dataset
            load([pn.dataIn, sprintf('%s_%s_FFT.mat', info.IDs_all{indID}, cond_names{indCond})],'freq');
            freqMerge{indID,indCond} = freq;
        end
    end

    cfg = [];
    cfg.keepindividual = 'yes';
    [grandavg_EC] = ft_freqgrandaverage(cfg, freqMerge{:,1});
    [grandavg_EO] = ft_freqgrandaverage(cfg, freqMerge{:,2});

    save([pn.dataOut, 'B_FFT_grandavg_individual.mat'], 'grandavg_EC', 'grandavg_EO', 'info')

end