%% plot results of CBPA: frequency*channel

    restoredefaultpath;
    clear all; close all; pack; clc;

%% set up paths

    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
    pn.dataIn   = [pn.root, 'B_analyses/B_FFT/B_data/A_spectra/'];
    pn.dataOut  = [pn.root, 'B_analyses/B_FFT/B_data/'];
    pn.tools    = [pn.root, 'B_analyses/A_MSE_CSD_multiVariant/T_tools/'];
    addpath([pn.tools, 'fieldtrip-20170904/']);
    ft_defaults

%% load stat output

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat')

%% Supplementary Figure  plot stat output

    h = figure('units','normalized','position',[.1 .1 .4 .4]);
        imagesc(stat{4,1}.stat, 'AlphaData', .5); 
        hold on;
        imagesc(stat{4,1}.mask.*stat{4,1}.stat, 'AlphaData', stat{4,1}.mask); 
        set(gca, 'XTick', 1:5:41);
        set(gca, 'XTickLabels', round(stat{1,1}.freq(get(gca, 'XTick')),2));
        set(gca, 'YTickLabels', []);
        xlabel('Frequency (Hz)'); ylabel('Channels (anterior-posterior)');
        cb = colorbar; set(get(cb,'title'),'string','t values');
        title({'Spectral power during rest:';'Older adults > Younger adults'});
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/C_figures/';
    figureName = 'C2_CBPA_EO_OAvsYA';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')
    export_fig([pn.plotFolder, figureName, '.pdf'], '-transparent')

%% Supplementary: plot original power data (not difference) for comparison

load([pn.dataOut, 'B_FFT_grandavg_individual.mat'], 'grandavg_EC', 'grandavg_EO', 'info')

ageIdx{1} = find(cellfun(@str2num, info.IDs_all, 'un', 1)<2000);
ageIdx{2} = find(cellfun(@str2num, info.IDs_all, 'un', 1)>=2000);

conds = {'EC'; 'EO'};

for indAge = 1:2
    for indCond = 1:2
        CBPAstruct{indAge,indCond} = eval(['grandavg_', conds{indCond}]);
        CBPAstruct{indAge,indCond}.powspctrm = CBPAstruct{indAge,indCond}.powspctrm(ageIdx{indAge},:,:);
    end
end

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); hold on;
    plot(squeeze(nanmedian(nanmean(CBPAstruct{1,1}.powspctrm(:,44:60,:),2),1)), 'LineWidth', 2)
    plot(squeeze(nanmedian(nanmean(CBPAstruct{2,1}.powspctrm(:,44:60,:),2),1)), 'LineWidth', 2)
    ylabel('Median power (a.u.)'); xlabel('Frequency (Hz)');
    title('Eyes Closed');
    set(gca, 'XTick', 1:4:41);
    set(gca, 'XTickLabels', round(CBPAstruct{1,1}.freq(get(gca, 'XTick')),2));
    xlim([1,41]);
    legend({'YA', 'OA'}); legend('boxoff');
subplot(1,2,2); hold on;
    plot(squeeze(nanmedian(nanmean(CBPAstruct{1,2}.powspctrm(:,44:60,:),2),1)), 'LineWidth', 2)
    plot(squeeze(nanmedian(nanmean(CBPAstruct{2,2}.powspctrm(:,44:60,:),2),1)), 'LineWidth', 2)
    ylabel('Median power (a.u.)'); xlabel('Frequency (Hz)');
    title('Eyes Open');
    set(gca, 'XTick', 1:4:41);
    set(gca, 'XTickLabels', round(CBPAstruct{1,1}.freq(get(gca, 'XTick')),2));
    xlim([1,41]);
    legend({'YA', 'OA'}); legend('boxoff');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% Supplementary: plot in log10 power

    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    subplot(1,2,1); hold on;
        plot(squeeze(nanmedian(nanmean(log10(CBPAstruct{1,1}.powspctrm(:,44:60,:)),2),1)), 'LineWidth', 2)
        plot(squeeze(nanmedian(nanmean(log10(CBPAstruct{2,1}.powspctrm(:,44:60,:)),2),1)), 'LineWidth', 2)
        ylabel('Median power (log10)'); xlabel('Frequency (Hz)');
        title('Eyes Closed');
        set(gca, 'XTick', 1:4:41);
        set(gca, 'XTickLabels', round(CBPAstruct{1,1}.freq(get(gca, 'XTick')),2));
        xlim([1,41]);
        legend({'YA', 'OA'}); legend('boxoff');
    subplot(1,2,2); hold on;
        plot(squeeze(nanmedian(nanmean(log10(CBPAstruct{1,2}.powspctrm(:,44:60,:)),2),1)), 'LineWidth', 2)
        plot(squeeze(nanmedian(nanmean(log10(CBPAstruct{2,2}.powspctrm(:,44:60,:)),2),1)), 'LineWidth', 2)
        ylabel('Median power (log10)'); xlabel('Frequency (Hz)');
        title('Eyes Open');
        set(gca, 'XTick', 1:4:41);
        set(gca, 'XTickLabels', round(CBPAstruct{1,1}.freq(get(gca, 'XTick')),2));
        xlim([1,41]);
        legend({'YA', 'OA'}); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% Supplement: topographies: 8-12 Hz; 32-64 Hz

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [];

    figure;
    subplot(2,2,1)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 12;
        plotData.powspctrm = squeeze(nanmean(nanmean(CBPAstruct{1,2}.powspctrm(:,:,idxFreqs),3),1))';
        ft_topoplotER(cfg,plotData);
        title('8-12 Hz, YA')
    subplot(2,2,3)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
        plotData.powspctrm = squeeze(nanmean(nanmean(CBPAstruct{1,2}.powspctrm(:,:,idxFreqs),3),1))';
        ft_topoplotER(cfg,plotData);
        title('32-64 Hz, YA')
    subplot(2,2,2)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 12;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        title('8-12 Hz YA > OA')
    subplot(2,2,4)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        title('32-64 Hz YA > OA')
    
%% Supplement: topographies: 8-12 Hz; 32-64 Hz

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [-3 3];

    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    subplot(1,2,1)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 15;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
        cfg.highlightcolor = [1 0 0];
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        title('8-15 Hz OA > YA')
    subplot(1,2,2)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
        cfg.highlightcolor = [1 0 0];
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        title('32-64 Hz OA > YA')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/C_figures/';
    figureName = 'C2_toposEO_YAvsOA';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
