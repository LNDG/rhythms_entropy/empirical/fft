Scripts for the analysis of FFT data
-
**Description of scripts**
-
A:  
- run FFT on CSD-transformed resting state data

B:   
- merge individual FFT data into global structures

C:
- perform cluster-based permutation analysis
- C2: plot results of cluster-based permutation analysis

D:
- calculate PSD slopes
